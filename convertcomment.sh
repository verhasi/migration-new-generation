#!/bin/bash
#streamUUID the UUID of the stream
#changesetUUID the UUID of the changeset
if [ "$streamUUID" = "" ]
then
	echo ERROR no stream defined in streamUUID
	exit 1
fi

if [ "$componentUUID" = "" ]
then
	echo ERROR no component defined in componentUUID
	exit 1
fi

if [ "$changesetUUID" = "" ]
then
	echo ERROR no changeset defined in changesetUUID
	exit 1
fi

mkdir -p "data/$streamUUID/$componentUUID/$citnetenvironment"

lookedupfilename="data/lookedup-$streamUUID-$componentUUID.txt"
incommentfilename="data/$streamUUID/$componentUUID/comment-$changesetUUID.txt"
outcommentfilename="data/$streamUUID/$componentUUID/$citnetenvironment/comment-$changesetUUID.txt"

replace(){
	awk 'BEGIN{FS=": "};FNR==NR{a[$1]=$2;next};/^[0-9]+: /{if(a[$1])$1=a[$1]":"};{print $0}' $lookedupfilename $incommentfilename > $outcommentfilename
}

if [ -f  $lookedupfilename ] && [ $(cat $lookedupfilename|wc -l) -gt 0  ]
then
	replace
else
	cp $incommentfilename $outcommentfilename	
fi

