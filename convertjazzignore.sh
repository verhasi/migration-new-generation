#/bin/bash
if [ "$citnetgitauthor" = "" ]
then
	echo ERROR no git author defined in citnetgitauthor >&2
	exit 1
fi
if [ "$streamUUID" = "" ]
then
	echo ERROR no stream defined in streamUUID >&2
	exit 1
fi
if [ "$componentUUID" = "" ]
then
	echo ERROR no component defined in componentUUID >&2
	exit 1
fi

gitdir="git-data/$streamUUID/$componentUUID/.git"
pushd $gitdir/..
git pull
groovy ../../../convertjazzignore.groovy
git add --all
git commit --author="$citnetgitauthor" --message "All .jazzignore file has been converted to .gitignore file."
git tag "RTC2CITnet-migration"
git push origin master
git push --tags
popd
