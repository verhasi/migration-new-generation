#!/bin/bash
#streamUUID the UUID of the stream
#changesetUUID the UUID of the changeset
if [ "$streamUUID" = "" ]
then
	echo ERROR no stream defined in streamUUID
	exit 1
fi

if [ "$componentUUID" = "" ]
then
	echo ERROR no component defined in componentUUID
	exit 1
fi

if [ "$changesetUUID" = "" ]
then
	echo ERROR no changeset defined in changesetUUID
	exit 1
fi

jsondir="jazz-json/$streamUUID/$componentUUID"
mkdir -p "data/$streamUUID/$componentUUID"

metadatafilename="data/$streamUUID/$componentUUID/metadata-$changesetUUID.txt"
cat $jsondir/changesetdto-$changesetUUID.json|$COMMONS/jq/jq --raw-output '
"author "+.author.name+" <"+.author.emailAddress+"> "+(.dateModified[0:19]+"Z"|fromdate|tostring),
"committer "+.addedBy.name+" <"+.addedBy.emailAddress+"> "+(.dateAdded[0:19]+"Z"|fromdate|tostring)'>$metadatafilename

commentfilename="data/$streamUUID/$componentUUID/comment-$changesetUUID.txt"
cat $jsondir/changesetdto-$changesetUUID.json|$COMMONS/jq/jq --raw-output '
.reasons[]?.label,if .comment!="" then .comment else empty end' >$commentfilename

tobelookedupfilename="data/tobelookedup-$streamUUID-$componentUUID.txt"
cat $jsondir/changesetdto-$changesetUUID.json|$COMMONS/jq/jq --raw-output '
.reasons[]?.location|split("/")[2]' >>$tobelookedupfilename
