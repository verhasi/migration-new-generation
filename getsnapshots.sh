#!/bin/bash
#streamUUID the UUID of the stream
#componentUUID the UUID of the component
if [ "$streamUUID" = "" ]
then
        echo ERROR no stream defined in streamUUID
        exit 1
fi
export https_proxy=
cookiesfile="cookies/cookies-snapshots-$streamUUID.txt"
jazzsnapshotfile="jazz-json/snapshots-$streamUUID.txt"
snapshotsfile="data/snapshots-$streamUUID.txt"
commonparams="--insecure --cookie-jar $cookiesfile --silent --show-error"

. ./credentials.sh
./login.sh "-snapshots-$streamUUID"
curl --cookie $cookiesfile $commonparams $hostbase'service/com.ibm.team.scm.common.internal.rest.IScmRestService/baselineSets?workspaceItemId='$streamUUID -H 'accept: text/json' -H 'Content-Type: application/x-www-form-urlencoded' -H 'X-Requested-With: XMLHttpRequest' >$jazzsnapshotfile

cat $jazzsnapshotfile |$COMMONS/jq/jq --raw-output '."soapenv:Body".response.returnValue.values[]|.name+"|"+.baselines[].itemId' > $snapshotsfile

