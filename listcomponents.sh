#/bin/bash

scm=$COMMONS/jazz/scmtools/eclipse/scm.sh
n=1
cat projectareas.txt|sed 's/^[^:]*:\([^)]*\)).*$/\1/'|while read projectAreaUUID; do
	projectdir="data/$projectAreaUUID"
        cat  "$projectdir/streams.$projectAreaUUID.txt" |while read streamline; do
		streamUUID=`echo -n $streamline|sed 's/^[^:]*:\([^)]*\)).*$/\1/'`
		streamdir="$projectdir/$streamUUID"
		rm -rf $streamdir
		mkdir -p $streamdir
		$scm --show-uuid y list components -r rtc --maximum all $streamUUID > "data/$projectAreaUUID/$streamUUID/components.$projectAreaUUID.$streamUUID.txt"
		echo -n "." 
	let n++
        done
done

