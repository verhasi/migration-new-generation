#/bin/bash
# cat ../../../data/baselines-_n-e1N45zEd-qwNa7C2MPrg.txt |cut -f2 -d'|'|/ec/local/citnet/commons/parallel/bin/parallel   "git log --max-count 1 --before {} --pretty=short | head -n1 | cut -f2 -d' '"|/ec/local/citnet/commons/parallel/bin/parallel git tag
if [ "$streamUUID" = "" ]
then
	echo ERROR no stream defined in streamUUID >&2
	exit 1
fi
if [ "$componentUUID" = "" ]
then
	echo ERROR no component defined in componentUUID
	exit 1
fi
gitdir="git-data/$streamUUID/$componentUUID/.git"
baselinesfile="../../../data/baselines-$componentUUID.txt"
pushd $gitdir/..
cat $baselinesfile | sort -k2 -n --field-separator='|' | while IFS='|' read tagname tagdate baselineUUID; do
	tagcommit=$(git log --max-count 1 --before $tagdate --pretty=short | head -n1 | cut -f2 -d' ')
	#echo "DEBUG tagname=$tagname tagdate=$tagdate tagcommit=$tagcommit"
	tagname=$(echo $tagname|sed 's/ /_/g')
	git tag -f "$tagname" "$tagcommit"
done
git push origin --tags
popd
