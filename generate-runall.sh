streamsdir=data/$projectareaUUID/
streamfile=$streamsdir/streams.$projectareaUUID.txt
rm $streamsdir/runall-ALL.sh

cat >$streamsdir/createrepos.sh << EOF
#/bin/bash
jiradminuser="REPLACE"
jiradminpass='REPLACE'
hostbase=https://webgate.?test/acceptance/?.ec.europa.eu/CITnet/stash
projectname=REPLACE
EOF
cp $streamsdir/createrepos.sh $streamsdir/deleterepos.sh

filter(){
	echo "$1"|sed 's/"//g;s/[^a-zA-Z0-9 -_.]/_/g'
}
cat $streamfile|while read streamUUID  streamname ;do
	streamname=$(echo $streamname|sed 's/\(".*"\).*/\1/')
	streamUUID=$(echo $streamUUID|sed 's/^[^:]*:\([^)]*\)).*$/\1/')
	runallfilename=runall-$streamUUID.sh
	echo "./$runallfilename #$streamname">>$streamsdir/runall-ALL.sh
	componentsdir=$streamsdir/$streamUUID
	componentfile=$componentsdir/components.$projectareaUUID.$streamUUID.txt
	streamname=$(filter "$streamname")
	if [ ${#streamname} -gt 128  ] 
	then
		echo "WARNING too long stream name $streamname."
	else
		echo "OK stream name $streamname."
	fi	
	rm -f "$streamsdir/$runallfilename"
	cat $componentfile|grep Component|while read null componentUUID componentname;do
		componentUUID=$(echo $componentUUID|sed 's/^[^:]*:\([^)]*\)).*$/\1/')
		componentname=$(filter "$componentname")
#	        echo "stream  is $streamUUID $streamname. component is $componentUUID $componentname"
		echo './execute.sh '$streamUUID' '$componentUUID' "'$streamname'" "'$componentname'"'>>"$streamsdir/$runallfilename"
		echo 'curl -X POST -u $jiradminuser:$jiradminpass -H "X-Atlassian-Token: no-check" -H "Content-Type: application/json"  $hostbase/rest/api/1.0/projects/$projectname/repos -d '"'"'{"name":"'"$streamname - $componentname"'","scmId":"git","forkable":true}'"'">>$streamsdir/createrepos.sh
		echo 'curl -X DELETE -u $jiradminuser:$jiradminpass -H "X-Atlassian-Token: no-check"  $hostbase/rest/api/1.0/projects/$projectname/repos/'$(echo "$streamname - $componentname"|sed 's/ /-/g')>>$streamsdir/deleterepos.sh
	done
done

