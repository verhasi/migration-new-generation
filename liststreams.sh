#/bin/bash

scm=$COMMONS/jazz/scmtools/eclipse/scm.sh
cat projectareas.txt|sed  's/^[^:]*:\([^)]*\)).*$/\1/'|while read projectAreaUUID; do
	projetdir="data/$projectAreaUUID"
	rm -rf $projetdir
	mkdir -p $projetdir
        $scm --show-uuid y list streams -r rtc --maximum all --projectarea $projectAreaUUID > "$projetdir/streams.$projectAreaUUID.txt"
done

