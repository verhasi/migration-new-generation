#!/bin/bash
#streamUUID the UUID of the stream
#componentUUID the UUID of the component
if [ "$componentUUID" = "" ]
then
        echo ERROR no component defined in componentUUID
        exit 1
fi

cookiesfile="cookies/cookies-baselines-$componentUUID.txt"
jazzbaselinefile="jazz-json/baselines-$componentUUID.txt"
onepagefile=$jazzbaselinefile'.tmp'
baselinefile="data/baselines-$componentUUID.txt"
commonparams="--insecure --cookie-jar $cookiesfile --silent --show-error"

. ./credentials.sh
./login.sh "-baselines-$componentUUID"
echo '{"pages":[' >$jazzbaselinefile
lasttimestamp="-1"
while [ $lasttimestamp -ne 0 ]
do
	if [ $lasttimestamp -eq -1 ]
	then
		lasttimestampparam=''
	else
		lasttimestampparam='jazz_scm:lastTimestamp='$lasttimestamp'&'
	fi
	curl --cookie $cookiesfile $commonparams $hostbase'oslc-scm/baseline?'$lasttimestampparam'jazz_scm:componentId='$componentUUID -H 'Content-Type: application/x-www-form-urlencoded' -H 'X-Requested-With: XMLHttpRequest' -H 'X-Method-Override: REPORT' --data '{"propertyRequest":{"jazz_scm:lastModified":null,"jazz_scm:creator":{"dcterms:name":null,"rdf:resource":null},"dcterms:created":null}}' >$onepagefile
	lasttimestamp=$(cat $onepagefile|$COMMONS/jq/jq --raw-output '."jazz_scm:lastTimestamp"')
	cat $jazzbaselinefile'.tmp' >> $jazzbaselinefile
	if [ $lasttimestamp -ne 0 ]
	then
		echo ',' >> $jazzbaselinefile
	fi
done

echo ']}'>> $jazzbaselinefile

cat $jazzbaselinefile |/ec/local/citnet/commons/jq/jq --raw-output '.pages[]."jazz_scm:results"[]|if(."jazz_scm:creator"."dcterms:name"|startswith("DIGIT METHODO")) then empty  else . end|."dcterms:name"+"|"+(."dcterms:created"/1000|floor|tostring)+"|"+."jazz_scm:itemId"'|sort > $baselinefile
