#/bin/bash
#streamUUID the UUID of the stream
#componentUUID the UUID of the component
if [ "$streamUUID" = "" ]
then
        echo ERROR no stream defined in streamUUID
        exit 1
fi
if [ "$componentUUID" = "" ]
then
        echo ERROR no component defined in componentUUID
        exit 1
fi
parallel=$COMMONS/parallel/bin/parallel
export jirausername=$(eval echo \${jira${citnetenvironment}username})
export jirapassword=$(eval echo \${jira${citnetenvironment}password})
export lookedup="data/lookedup-$streamUUID-$componentUUID.txt"
export failedlookedup="data/failedlookedup-$streamUUID-$componentUUID.txt"
export jirabaseurl
case $citnetenvironment in
	"test"|"tst")
		jirabaseurl="https://webgate.test.ec.europa.eu/CITnet/jira"
	;;
	"acc")
		jirabaseurl="https://webgate.acceptance.ec.europa.eu/CITnet/jira"
	;;
	"prod"|"prd")
		jirabaseurl="https://webgate.ec.europa.eu/CITnet/jira"
	;;
	*)
	 
esac
lookup(){
	result=$(curl --silent -u $jirausername:$jirapassword $jirabaseurl'/rest/api/2/search?jql=%22RTC%20Id%22~'$1'&fields=key' | $COMMONS/jq/jq --raw-output  '.issues[].key')
	if [ "$result" == "" ]
	then
		echo "$1" >> $failedlookedup
	else
		echo "$1: $result" >> $lookedup
	fi
}
export -f lookup

rm -f $lookedup
touch $lookedup
rm -f $failedlookedup
touch $failedlookedup
cat "data/tobelookedup-$streamUUID-$componentUUID.txt"|sort -u|$parallel --eta lookup {}

