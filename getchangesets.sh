#!/bin/bash
#streamUUID the UUID of the stream
#componentUUID the UUID of the component
if [ "$streamUUID" = "" ]
then
	echo ERROR no stream defined in streamUUID
	exit 1
fi
if [ "$componentUUID" = "" ]
then
	echo ERROR no component defined in componentUUID
	exit 1
fi

cookiesfile="cookies/cookies-changesets-$streamUUID-$componentUUID.txt"

commonparams="--insecure --cookie-jar $cookiesfile --silent --show-error"

jazzlogin(){
	curl $commonparams "${hostbase}secure/authenticated/identity" --output /dev/null
	curl $commonparams --location --cookie $cookiesfile --data j_username=$jazzusername --data j_password=$jazzpassword ${hostbase}secure/authenticated/j_security_check --output /dev/null
}

#gets one page of change sets 
#$1 strem UUID
#$2 component UUID
#$3 the last changeset UUID if not the first page
jazzgetchangesets(){
	curl $commonparams --cookie $cookiesfile ${hostbase}'service/com.ibm.team.scm.common.internal.rest.IScmRestService2/historyPlus?'$3'&path=workspaceId%2F'$1'%2FcomponentId%2F'$2  -H 'User-Agent: vibrowser' -H 'Content-Type: application/x-www-form-urlencoded; charset=utf-8' -H 'accept: text/json'
}

splitpagefile(){
	cat $jsonpagefilename|/ec/local/citnet/commons/jq/jq --raw-output '."soapenv:Body".response.returnValue.value.changeSets[].changeSetDTO.itemId'|while read changesetUUID ; do
		cat $jsonpagefilename|/ec/local/citnet/commons/jq/jq --raw-output '."soapenv:Body".response.returnValue.value.changeSets[].changeSetDTO|select(.itemId=="'$changesetUUID'")'>"$jsondir/changesetdto-$changesetUUID.json"
	done
}

jsondir=jazz-json/$streamUUID/$componentUUID
mkdir -p $jsondir
rm -f $jsondir/changesets-*.json
jazzlogin
pageNum=1
gotAllChangeSets=1
resultfilename="data/changesets-$streamUUID-$componentUUID.txt"
echo -n >$resultfilename
while [ $gotAllChangeSets -ne 0 ] 
do
	jsonpagefilename=$jsondir/changesets-$pageNum.json
	prevLastChangeSet=$lastChangeSet
	
	if [ $pageNum -ne 1 ] 
	then
		jazzgetchangesets $streamUUID $componentUUID 'last='$lastChangeSet >$jsonpagefilename
	else
		jazzgetchangesets $streamUUID $componentUUID >$jsonpagefilename
	fi
	splitpagefile	
	lastChangeSet=$(cat $jsonpagefilename|/ec/local/citnet/commons/jq/jq --raw-output '."soapenv:Body".response.returnValue.value.changeSets[-1]."changeSetDTO".itemId')
	
	if [ "$lastChangeSet" = "$prevLastChangeSet" ] 
	then
		gotAllChangeSets=0
	fi 

let pageNum++
#read
done

ls -tr $jsondir/changesets-*.json|xargs -n1 cat|/ec/local/citnet/commons/jq/jq --raw-output '."soapenv:Body".response.returnValue.value.changeSets[]."changeSetDTO".itemId'|uniq|tac >$resultfilename

