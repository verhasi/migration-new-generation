#!/bin/bash
#streamUUID the UUID of the stream
#componentUUID the UUID of the component
if [ "$streamUUID" = "" ]
then
	echo ERROR no stream defined in streamUUID >&2
	exit 1
fi
if [ "$componentUUID" = "" ]
then
	echo ERROR no component defined in componentUUID >&2
	exit 1
fi
if [ "$citnetenvironment" = "" ]
then
        echo ERROR no CITnet environment defined in citnetenvironment >&2
        exit 1
fi
export streamUUID
export componentUUID

. ./credentials.sh
gitdir="git-data/$streamUUID/$componentUUID/.git"
mkdir -p $gitdir
git --git-dir=$gitdir init
./writefastimport.sh |git --git-dir=$gitdir fast-import
pushd $gitdir/..
git reset --hard master
popd

