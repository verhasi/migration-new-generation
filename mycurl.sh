mkdir -p cookies
if [ ! -f "cookies/cookies$2.txt" ] || [ $(find cookies -name "cookies$2.txt" -mmin +59) ]
then
. ./login.sh $2
fi
for i in $1; do
	if [ "$i" == "cookies.txt"  ]
	then
		param="$param cookies/cookies$2.txt" 
	else
		param="$param $i"
	fi
done
curl -H 'Accept-encoding: gzip' $param
