#/bin/bash
environment=.acceptance
projectname=rtdcompass
hostbase=https://webgate${environment}.ec.europa.eu/CITnet/stash

if [ "$jiradminuser" = "" ]
then
        echo ERROR no user with admin access to Jira defined in jiradminuser
        exit 1
fi
if [ "$jiradminpass" = "" ]
then
        echo ERROR no password for jiradminuser defined in jiradminpass
        exit 1
fi
curl -X GET -u $jiradminuser:$jiradminpass -H "X-Atlassian-Token: no-check" $hostbase/rest/api/1.0/projects/$projectname'/repos?limit=250'|$COMMONS/jq/jq '.values[].slug'|sed 's/"//g'|while read reponame ; do
curl -X DELETE -u $jiradminuser:$jiradminpass -H "X-Atlassian-Token: no-check"  $hostbase/rest/api/1.0/projects/$projectname/repos/$reponame
done

