#!/bin/bash
#streamUUID the UUID of the stream
#componentUUID the UUID of the component
if [ "$streamUUID" = "" ]
then
	echo ERROR no stream defined in streamUUID
	exit 1
fi
if [ "$componentUUID" = "" ]
then
	echo ERROR no component defined in componentUUID
	exit 1
fi
export streamUUID
export componentUUID

parallel=$COMMONS/parallel/bin/parallel
changesetsfilename="data/changesets-$streamUUID-$componentUUID.txt"
echo Replace WIs to JIRA keys in comments
cat $changesetsfilename|$parallel --eta  env changesetUUID={} ./convertcomment.sh
